﻿using System;
using System.Numerics;

namespace Lab3
{
    internal class Program
    {
        static double findU(double s, double r)
        {
            double u;

            u = Math.Pow(s, r - 1) - Math.Pow(r, s - 1);
            u %= r * s;

            return u;
        }

        static bool tryDivideMethod(long num, int[] p)
        {
            for(int i = 0; i < p.Length; i++)
            {
                if(num % p[i] == 0)
                {
                    return false;
                }
            }

            return true;
        }
        
        static long findGreaterCommonDivizer(int a, int b)
        {
            if(b == 0)
            {
                return a;
            }
            return findGreaterCommonDivizer(b, a % b);
        }

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Green;

            BigInteger a = 193;
            
            Console.WriteLine(BigInteger.Pow(a, 141) % 1129);
            
            
            long GCD = findGreaterCommonDivizer(1128, 5);
            //Console.WriteLine(GCD);

            long n = 1129;
            int[] p = new int[] {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31};
            //Console.WriteLine(tryDivideMethod(n, p));
            
            double u = findU(47, 59);
            //Console.WriteLine(u);
            Console.ForegroundColor= ConsoleColor.White;
        }
    }
}
